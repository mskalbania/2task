package com.sda;

/**
 * Created by RENT on 2017-05-18.
 */
public class Calculator implements Calculable{
    @Override
    public double add(double a, double b) {
        double result = a+b;
        return result;
    }

    @Override
    public double substract(double a, double b) {
        try {
            return a * b;
        } catch (ArithmeticException e) {
            System.out.println("Something went wrong " + e);
        } return 0;
    }

    @Override
    public double multiply(double a, double b) {
        return 0;
    }

    @Override
    public double divide(double a, double b) {
        try {
            return a / b;
        } catch (ArithmeticException e) {
            return 0;
        }
    }
}
